use gl;
use std::ffi::CString;
use std::ops::Drop;
use std::ptr;

#[derive(Clone)]
pub struct Shader {
    name: String,
    id: u32,
}
impl Shader {
    pub fn new<T: Into<String>>(name: T, id: u32) -> Shader {
        Shader {
            name: name.into(),
            id,
        }
    }
    pub fn name(&self) -> &String {
        &self.name
    }
    pub fn set_name<T: Into<String>>(&mut self, s: T) {
        self.name = s.into()
    }
    pub fn id(&self) -> u32 {
        self.id
    }
    pub fn bind(&self) {
        unsafe {
            gl::UseProgram(self.id);
        }
    }
    pub fn unbind(&self) {
        unsafe {
            gl::UseProgram(0);
        }
    }
}
impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            gl::DeleteProgram(self.id);
        }
    }
}

#[macro_export]
macro_rules! shader_program {
    ($x:expr, $y:expr, $z:expr) => {
        ::shaders::Shader::new(
            $x,
            ::shaders::gen_shader_programme(
                &include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), $y))[..],
                &include_bytes!(concat!(env!("CARGO_MANIFEST_DIR"), $z))[..],
            ),
        )
    };
}

pub fn gen_shader_programme<T: Into<Vec<u8>>>(vertex: T, fragment: T) -> u32 {
    unsafe {
        let shader_programme = gl::CreateProgram();
        let vertex = gen_shader(vertex, ShaderType::Vertex);
        let fragment = gen_shader(fragment, ShaderType::Fragment);
        gl::AttachShader(shader_programme, vertex);
        gl::AttachShader(shader_programme, fragment);
        gl::LinkProgram(shader_programme);
        gl::DetachShader(shader_programme, vertex);
        gl::DetachShader(shader_programme, fragment);
        gl::DeleteShader(vertex);
        gl::DeleteShader(fragment);
        shader_programme
    }
}

enum ShaderType {
    Vertex,
    Fragment,
}
///Compiles the shader
fn gen_shader<T: Into<Vec<u8>>>(code: T, shader_type: ShaderType) -> u32 {
    let c_str = CString::new(code).unwrap();
    unsafe {
        let shader = {
            match shader_type {
                ShaderType::Vertex => gl::CreateShader(gl::VERTEX_SHADER),
                ShaderType::Fragment => gl::CreateShader(gl::FRAGMENT_SHADER),
            }
        };
        gl::ShaderSource(shader, 1, &c_str.as_ptr(), ptr::null());
        gl::CompileShader(shader);
        shader
    }
}
