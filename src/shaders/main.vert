#version 400
in vec2 a_Pos;
uniform vec2 u_Offset;

void main() {
    gl_Position = vec4(a_Pos.x + u_Offset.x, a_Pos.y + u_Offset.y, 0.0, 1.0);
}