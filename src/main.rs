extern crate gl;
extern crate glutin;
extern crate specs;
#[macro_use]
mod shaders;
use glutin::GlContext;
use specs::prelude::*;
use std::ffi::CString;
use std::os::raw::c_void;

#[derive(Debug)]
struct Offset([f32; 2]);

impl Component for Offset {
    type Storage = VecStorage<Self>;
}

struct SysA {
    shader: u32,
}

impl<'a> System<'a> for SysA {
    type SystemData = ReadStorage<'a, Offset>;
    fn run(&mut self, mut pos: Self::SystemData) {
        for p in pos.join() {
            unsafe {
                gl::UseProgram(self.shader);
                let uniform_position = || unsafe {
                    gl::GetUniformLocation(self.shader, CString::new("u_Offset").unwrap().as_ptr())
                };
                gl::Uniform2f(uniform_position(), p.0[0], p.0[1]);
                gl::DrawArrays(gl::TRIANGLES, 0, 3);
            }
        }
    }
}

fn main() {
    let window_builder = glutin::WindowBuilder::new()
        .with_title("Hello world")
        .with_dimensions(glutin::dpi::LogicalSize::new(800.0, 600.0));

    let events_loop = glutin::EventsLoop::new();

    let context = glutin::ContextBuilder::new();

    let window = glutin::GlWindow::new(window_builder, context, &events_loop).unwrap();

    unsafe {
        window.make_current().unwrap();
    }
    gl::load_with(|s| window.get_proc_address(s) as *const _);

    let shader = shader_program!("Main", "/src/shaders/main.vert", "/src/shaders/main.frag");
    unsafe {
        gl::ClearColor(0.3, 0.3, 0.3, 1.0);
    }

    let triangle: [f32; 6] = [-0.1, -0.1, 0.0, 0.1, 0.1, -0.1];

    shader.bind();
    let mut vao = 0;
    let mut vbo = 0;

    let mut world = World::new();
    world.register::<Offset>();
    world.create_entity().with(Offset([0.3, 0.3])).build();
    world.create_entity().with(Offset([0.2, 0.7])).build();
    world.create_entity().with(Offset([0.0, 0.0])).build();
    world.create_entity().with(Offset([-1.0, 0.0])).build();

    let mut dispatcher = DispatcherBuilder::new()
        .with(
            SysA {
                shader: shader.id(),
            },
            "sys_a",
            &[],
        ).build();
    dispatcher.setup(&mut world.res);

    unsafe {
        gl::GenVertexArrays(1, &mut vao);
        gl::BindVertexArray(vao);
        gl::GenBuffers(1, &mut vbo);
        gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
        gl::BufferData(
            gl::ARRAY_BUFFER,
            4 * triangle.len() as isize,
            &triangle[0] as *const f32 as *const c_void,
            gl::STATIC_DRAW,
        );
        gl::VertexAttribPointer(0, 2, gl::FLOAT, gl::FALSE, 2 * 4, std::ptr::null());
        gl::EnableVertexAttribArray(0);
    }

    shader.bind();
    loop {
        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT);
            dispatcher.dispatch_seq(&mut world.res);
        }
        window.swap_buffers().unwrap()
    }
}
